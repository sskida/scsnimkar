<?php //echo 'asasasasas';exit;?>


<?php get_header(); ?>


        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 single-course"  id="side-border">
            <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class(); ?>>
                <h1 class="entry-title" id="bottom-border"><?php the_title(); ?></h1>
                <?php //get_template_part('templates/entry-meta'); ?>
                
                    <?php 
                    the_post_thumbnail('full');
                    the_content(); 
                    ?>
                <div class="course-price">
	            	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	            		<h2><label>Coures Fees - <?php echo get_post_meta( get_the_ID(), 'courses_fee', true ). ' Rs.'; ?></label></h2>
	            	</div>
	            	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="text-align:right;">
	            		<h2><a href="<?php echo site_url().'/customer-info?cid='.get_the_ID();?>">Apply for course </a></h2>
	            	</div>
            	</div>   
            </article>
            <?php endwhile; ?>

        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 single-side" id="side-margin">
            <?php 
                $args = array(
                    'post_type' => 'courses',
                    'posts_per_page' => -1,
                    'post_status' => 'publish' 
                );
                $courses = get_posts($args);
                echo '<h2> All Courses </h2>';
                echo '<ul>';
                foreach( $courses as $course ) {
                    echo '<li><a href="'.get_permalink($course->ID).'">'.$course->post_title.'</a></li>';
                }
                echo '</ul>';
            ?>
        </div>


<?php get_footer(); ?>
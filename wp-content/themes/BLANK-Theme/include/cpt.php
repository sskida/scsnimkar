<?php

/**
 * resister custom post type Course
 */
add_action( 'init', 'register_courses_cpt' );
function register_courses_cpt() {
	$labels = array(
		'name'               => _x( 'Courses', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Course', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Courses', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Courses', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add Course', 'Whyus Point', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Course', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Course', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Course', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Courses', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Courses', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Courses', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Courses:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Courses found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Courses found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'courses' ),
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title','editor', 'thumbnail','page-attributes' )
	);

	register_post_type( 'courses', $args );
}

add_action( 'init', 'register_candidates_cpt' );
function register_candidates_cpt() {
	$labels = array(
		'name'               => _x( 'Candidates', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Candidate', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Candidates', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Candidates', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add Candidate', 'Whyus Point', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Candidate', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Candidate', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Candidate', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Candidates', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Candidates', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Candidates', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Candidates:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Candidates found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Candidates found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'candidates' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title','editor', 'thumbnail','page-attributes' )
	);

	register_post_type( 'candidates', $args );
}

add_action( 'init', 'register_batches_cpt' );
function register_batches_cpt() {
	$labels = array(
		'name'               => _x( 'Batches', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Batches', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Batches', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Batches', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add Batches', 'Whyus Point', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Batch', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Batch', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Batch', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Batches', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Batches', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Batches', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Batches:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Batches found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Batches found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'batches' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title','editor', 'thumbnail','page-attributes' )
	);

	register_post_type( 'batches', $args );
}



add_action( 'init', 'register_packages_cpt' );
function register_packages_cpt() {
	$labels = array(
		'name'               => _x( 'Packages', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Package', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Packages', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Packages', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add Package', 'Whyus Point', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Package', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Package', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Package', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Package', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Packages', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Packages', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Packages:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Packages found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Packages found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'packages' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title','editor', 'thumbnail','page-attributes' )
	);

	register_post_type( 'packages', $args );
}

add_action( 'init', 'register_circuits_cpt' );
function register_circuits_cpt() {
	$labels = array(
		'name'               => _x( 'Circuits', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Circuit', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Circuits', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Circuits', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add Circuit', 'Whyus Point', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Circuit', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Circuit', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Circuit', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Circuit', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Circuits', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Circuits', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Circuits:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Circuits found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Circuits found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'circuits' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title','editor', 'thumbnail','page-attributes' )
	);

	register_post_type( 'circuits', $args );
}
?>
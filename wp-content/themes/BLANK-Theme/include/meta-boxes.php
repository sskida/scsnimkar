<?php

// siddharthdes@gmail.com 65313233	
/****
 * Meta box code here
 */


add_action( 'add_meta_boxes', 'add_course_cost_meta' );  
function add_course_cost_meta()  
{  
  	global $post;
  	if( $post->post_type == 'courses'){
    		add_meta_box( 'courses-meta-box-id', 'Course Fee', 'courses_meta_box_cb', 'courses', 'normal', 'high');	
	} 
} 
function courses_meta_box_cb(){
	global $post; 
	$courses = get_post_meta($post->ID,'courses_fee',true);
	?>
	<p><label for="courses_fee">Course Fee</label></p> 
    <input type="text" name="courses_fee" id="courses_fee" value="<?php echo $courses; ?>" /> Rs. 

<?php
}
add_action('save_post','save_courses_fee_meta');
function save_courses_fee_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['courses_fee'] ) ) { 
	echo $post_id;
		 $ss = update_post_meta( $post_id, 'courses_fee', $_POST['courses_fee'] ) ; 	
	 }
}




/*** meta boxes for candidates CPT ****/

///meta address
add_action( 'add_meta_boxes', 'add_candidates_address_meta' );  
function add_candidates_address_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-meta-box-id', 'Address', 'candidates_address_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_address_meta_box_cb() {
	global $post; 
	$can_address = get_post_meta($post->ID,'can_address',true);
	?>
	<p><label for="can_address">Address</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_address" id="can_address" value="<?php echo $can_address; ?>" /> 

<?php
}
add_action('save_post','save_candidates_address_meta');
function save_candidates_address_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_address'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_address', $_POST['can_address'] ) ; 	
	 }
}

/////////// meta street
add_action( 'add_meta_boxes', 'add_candidates_street_meta' );  
function add_candidates_street_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-street-meta-box-id', 'Street', 'candidates_street_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_street_meta_box_cb() {
	global $post; 
	$can_street = get_post_meta($post->ID,'can_street',true);
	?>
	<p><label for="can_street">Street</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_street" id="can_street" value="<?php echo $can_street; ?>" /> 

<?php
}
add_action('save_post','save_candidates_street_meta');
function save_candidates_street_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_street'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_street', $_POST['can_street'] ) ; 	
	 }
}



//////////// city
add_action( 'add_meta_boxes', 'add_candidates_city_meta' );  
function add_candidates_city_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-city-meta-box-id', 'City', 'candidates_city_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_city_meta_box_cb() {
	global $post; 
	$can_city = get_post_meta($post->ID,'can_city',true);
	?>
	<p><label for="can_city">City</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_city" id="can_city" value="<?php echo $can_city; ?>" /> 

<?php
}
add_action('save_post','save_candidates_city_meta');
function save_candidates_city_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_city'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_city', $_POST['can_city'] ) ; 	
	 }
}


//////////// meta postcode

add_action( 'add_meta_boxes', 'add_candidates_postcode_meta' );  
function add_candidates_postcode_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-postcode-meta-box-id', 'Postal Code', 'candidates_postcode_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_postcode_meta_box_cb() {
	global $post; 
	$can_postcode = get_post_meta($post->ID,'can_postcode',true);
	?>
	<p><label for="can_postcode">Postal Code</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_postcode" id="can_postcode" value="<?php echo $can_postcode; ?>" /> 

<?php
}
add_action('save_post','save_candidates_postcode_meta');
function save_candidates_postcode_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_postcode'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_postcode', $_POST['can_postcode'] ) ; 	
	 }
}



//////////////////////// email
add_action( 'add_meta_boxes', 'add_candidates_email_meta' );  
function add_candidates_email_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-email-meta-box-id', 'Email', 'candidates_email_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_email_meta_box_cb() {
	global $post; 
	$can_email = get_post_meta($post->ID,'can_email',true);
	?>
	<p><label for="can_email">Email</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_email" id="can_email" value="<?php echo $can_email; ?>" /> 

<?php
}
add_action('save_post','save_candidates_email_meta');
function save_candidates_email_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_email'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_email', $_POST['can_email'] ) ; 	
	 }
}

/////////////////////////
//////////////////////// course name
add_action( 'add_meta_boxes', 'add_candidates_course_meta' );  
function add_candidates_course_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-course-meta-box-id', 'Course', 'candidates_course_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_course_meta_box_cb() {
	global $post; 
	$can_course = get_post_meta($post->ID,'can_course',true);
	?>
	<p><label for="can_course">Course</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="can_course" id="can_course" value="<?php echo $can_course; ?>" /> 

<?php
}
add_action('save_post','save_candidates_course_meta');
function save_candidates_course_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['can_course'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'can_course', $_POST['can_course'] ) ; 	
	 }
}

/////////////////////


add_action( 'add_meta_boxes', 'add_candidates_txnid_meta' );  
function add_candidates_txnid_meta()  
{  
  	global $post;
  	if( $post->post_type == 'candidates'){
    		add_meta_box( 'candidates-txnid-meta-box-id', 'Txnid', 'candidates_txnid_meta_box_cb', 'candidates', 'normal', 'high');	
	} 
} 
function candidates_txnid_meta_box_cb() {
	global $post; 
	$candidates_txnid = get_post_meta($post->ID,'candidates_txnid',true);
	?>
	<p><label for="candidates_txnid">Candidates Txnid</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="text" name="candidates_txnid" id="candidates_txnid" value="<?php echo $candidates_txnid; ?>" /> 

<?php
}
add_action('save_post','save_candidates_txnid_meta');
function save_candidates_txnid_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['candidates_txnid'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'candidates_txnid', $_POST['candidates_txnid'] ) ; 	
	 }
}








////////*************** for Batches post type ***************/

add_action( 'add_meta_boxes', 'add_batches_start_meta' );  
function add_batches_start_meta()  
{  
  	global $post;
  	if( $post->post_type == 'batches'){
    		add_meta_box( 'batches-start-meta-box-id', 'Batch Start', 'batch_start_meta_box_cb', 'batches', 'normal', 'high');	
	} 
} 
function batch_start_meta_box_cb() {
	global $post; 
	$batch_start = get_post_meta($post->ID,'batch_start',true);
	?>
	<p><label for="batch_start">Batch Start</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="date" name="batch_start" id="batch_start" value="<?php echo $batch_start; ?>" /> 

<?php
}
add_action('save_post','save_batch_start_meta');
function save_batch_start_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['batch_start'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'batch_start', $_POST['batch_start'] ) ; 	
	 }
}


///////////// 
add_action( 'add_meta_boxes', 'add_batches_end_meta' );  
function add_batches_end_meta()  
{  
  	global $post;
  	if( $post->post_type == 'batches'){
    		add_meta_box( 'batches-end-meta-box-id', 'Batch End', 'batch_end_meta_box_cb', 'batches', 'normal', 'high');	
	} 
} 
function batch_end_meta_box_cb() {
	global $post; 
	$batch_end = get_post_meta($post->ID,'batch_end',true);
	?>
	<p><label for="batch_end">Batch End</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="date" name="batch_end" id="batch_end" value="<?php echo $batch_end; ?>" /> 

<?php
}
add_action('save_post','save_batch_end_meta');
function save_batch_end_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['batch_end'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'batch_end', $_POST['batch_end'] ) ; 	
	 }
}




/******* meta for pakages ******/

add_action( 'add_meta_boxes', 'add_package_per_price' );  
function add_package_per_price()  
{  
  	global $post;
  	if( $post->post_type == 'packages'){
    		add_meta_box( 'packages-end-meta-box-id', 'Per Price', 'add_package_per_price_box_cb', 'packages', 'normal', 'high');	
	} 
} 
function add_package_per_price_box_cb() {
	global $post; 
	$package_per_price = get_post_meta($post->ID,'package_per_price',true);
	?>
	<p><label for="package_per_price">Per Price</label></p> 
	<!-- <textarea name="can_address" id="can_address"><?php echo $can_address; ?></textarea> -->
    <input type="number" name="package_per_price" id="package_per_price" value="<?php echo $package_per_price; ?>" /> 

<?php
}
add_action('save_post','save_package_per_price_meta');
function save_package_per_price_meta($post_id){
	
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		 return $post_id;
	} 
	if( isset( $_POST['package_per_price'] ) ) { 
	echo $post_id;
		 update_post_meta( $post_id, 'package_per_price', $_POST['package_per_price'] ) ; 	
	 }
}






?>

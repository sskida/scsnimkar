<?php 
/*
Template Name: Trainings
*/
?>

<?php get_header(); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
       <?php echo do_shortcode( '[rev_slider training]' ) ?>
    </div>
<?php 
    $args = array(
            'post_type' =>'courses',
            'posts_per_page' => -1
    );
    $courses = get_posts($args);	
    //echo '<pre>';print_r($courses);
?>
    <section id="courses-list">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:45px;">
        	<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10" >
        		<h2 id="training-title">List Of Course</h2>
                <?php foreach ($courses as $course) { ?>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <a href="<?php echo get_permalink( $course->ID ); ?>" class="course-title">
                        <div class="wpb_call_to_action wpb_content_element vc_clearfix cta_align_right course">
                            <h4 class="wpb_call_text"><?php echo $course->post_title; ?></h4>
                        </div>
                    </a>
                </div>
                <?php } ?>
        	</div>
        	
    	    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:20px;">
                <button style="  border: 0px;background-color: #FFF;" type="button" data-toggle="modal" data-target="#myModal">
  <img src="<?php echo get_template_directory_uri().'/images/shedule-button.png' ?>">
</button>

    	    </div>
	    </div>
	</section>
</div>
    

<?php
$today = date('Y-m-d');
$args = array(
    'meta_query' => array(
        array(
            'key' => 'batch_start',
            'compare'   => '>',
            'value'     => $today,
        )
    ),
    'post_type' => 'batches',
    'posts_per_page' => -1
);
$posts = get_posts($args);

//echo '<pre>';print_r($posts);


?>
	
    <!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Next Batch</h4>
      </div>
      <div class="modal-body">
        
            <div class="batch-time">
                <h4>Next batch schedule</h4>
                <?php if($posts) { 
                    $batch_end = get_post_meta($posts[0]->ID,'batch_end',true);
                    $batch_start = get_post_meta($posts[0]->ID,'batch_start',true);
                    ?>
                <p> <b>Start Date - <?php echo $batch_start; ?></b></p>
                <p><b>End Date - <?php echo $batch_end ;?></b></p>
                <?php }else { ?>
                <p><b>Next batch schedule not available</b></p>
                <?php }?>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php //get_sidebar(); ?>

<?php get_footer(); ?>
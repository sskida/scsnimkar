<!-- Web.Config Configuration File -->

<configuration>
    <system.web>
        <customErrors mode="Off"/>
		<compilation debug="true"/>
    </system.web>

<system.webServer>
   <httpErrors errorMode="Detailed"/>
   <asp scriptErrorSentToBrowser="true"/>
</system.webServer>
	
</configuration>
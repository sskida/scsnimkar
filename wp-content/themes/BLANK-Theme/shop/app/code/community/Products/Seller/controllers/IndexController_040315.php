<?php

class Products_Seller_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Root: ajaxlogin/ajax/index
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

 public function checkAction() {
    $now = time();
    $customerArray = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('group_id', 4)->getData();
    foreach ($customerArray as $customer) {
        if ($customer['paypal_active']) {
            $paypalDate = strtotime($customer['paypal_created_date']);
            $datediff = $now - $paypalDate;
            $dateDiff = floor($datediff / (60 * 60 * 24));
            if ($dateDiff >= 335 && $dateDiff < 365) {
                $toEmailID = $customer['email'];
                if ($toEmailID) {
                    $mail = new Zend_Mail();
                    $mailSubject = 'Paypal Subscription notification';
                    $mail->setBodyText('Your paypal subscription will be finished after one month. Please subscribe again to use our service');
                    $mail->setFrom('narayan.jadhav1988@gmail.com');
                    $mail->addTo($toEmailID);
                    $mail->setSubject($mailSubject);
                    $mail->send();
                }
            }
        }
    }
}

    public function createAction() {
        $this->loadLayout();
        $this->renderLayout();
        try {
            $client = new SoapClient(Mage::getBaseUrl() . 'index.php/api/soap/?wsdl');
            $session = $client->login('seller', 'seller');
            $arrParam = array('categories' => array($_POST['categories']),
                'manufacturer_name' => $_POST['manufacturer_name'],
                'manufacturer_number' => $_POST['manufacturer_number'],
                'name' => $_POST['product-name'],
                'weight' => $_POST['weight'],
                'status' => '1',
                'supplier_series_number' => $_POST['supplier-series-number'],
                'numbers_of_poles_connecter' => $_POST['number_of_poles_connecter'],
                'type' => $_POST['type'],
                'short_description' => $_POST['note'],
                'description' => $_POST['description'],
                'temperature_deg' => $_POST['temperature_deg'],
                'slit_unslit' => $_POST['slit_unslit'],
                'inner_dim' => $_POST['inner_dim'],
                'non_shrink' => $_POST['non_shrink'],
                'ratio' => $_POST['ratio'],
                'width' => $_POST['width'],
                'multicor' => $_POST['multicor'],
                'no_of_cores' => $_POST['no_of_cores'],
                'shielded_unshielded' => $_POST['shielded_unshielded'],
                'core_size' => $_POST['core_size'],
                'standard' => $_POST['standard'],
                'cable_size' => $_POST['cable_size'],
                'lenth' => $_POST['lenth'],
                'material' => $_POST['material'],
                'fuse' => $_POST['fuse'],
                'fuse_type' => $_POST['fuse_type'],
                'usage' => $_POST['usage'],
                'unit' => $_POST['unit'],
                'location' => $_POST['location'],
                'cable_type_standard' => $_POST['cable_type_standard'],
                'seller_email' => Mage::getSingleton('customer/session')->getCustomer()->getEmail(),
                'visibility' => '4',
                'price' => '0',
                'tax_class_id' => 4,
                'stock_data' => array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'min_sale_qty' => $_POST['mini-qty'], //Minimum Qty Allowed in Shopping Cart
                    'is_in_stock' => 1, //Stock Availability
                    'qty' => $_POST['qty'] //qty
                ),
            );
            
            srand(mktime());
            $result = $client->call($session, 'catalog_product.create', array('simple', 4, rand(), $arrParam));
         //   $result = $client->call($session, 'catalog_product.create', array('simple', 4, $_POST['sku'], $arrParam));

            $this->_redirect('seller?action=productCreated');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

 public function deleteAction() {
if(isset($_REQUEST['productId']) && $_REQUEST['productId']){
            $client = new SoapClient(Mage::getBaseUrl() . 'index.php/api/soap/?wsdl');
            $session = $client->login('seller', 'seller');
        $client->call($session, 'catalog_product.delete', trim($_REQUEST['productId']));
    }
$this->_redirectUrl($_SERVER["HTTP_REFERER"]);
}

    public function loginAction() {
        $customer = Mage::getModel("customer/customer");
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($_POST['email']);
        if (4 == $customer->getGroupId()) {
            $session = Mage::getSingleton('customer/session');
            $message = "wrong user name passward"; //error message to add to session
            if ($session->login($_POST['email'], $_POST['password'])) {
                $this->_redirect('seller?action=sellerProduct');
            } else {
                $this->_redirect('seller?action=sellerLogin');
            }
        } else {
            $this->_redirect('seller?sellerLogin&wrongLogin=1');
        }
    }

    public function sellerSuccessAction() {
        try {
            //echo '<pre>';
            if (isset($_REQUEST['rescode']) && $_REQUEST['rescode']=='0') {
                Mage::log($_REQUEST, null, 'seller.log');
                $customer = Mage::helper('customer')->getCustomer();
                $resource = Mage::getSingleton('core/resource');
                $writeConnection = $resource->getConnection('core_write');
                $table = $resource->getTableName('customer/entity');
                $dtCurrent = date('Y-m-d h:i:s');
                $query = "UPDATE {$table} SET paypal_active=1,paypal_created_date='{$dtCurrent}' WHERE entity_id = "
                        . (int) $customer->getEntityId();
                /**
                 * Execute the query
                 */
                $writeConnection->query($query);
				
            } 
            //var_dump($result);     
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function registerAction() {
        try {
            $client = new SoapClient(Mage::getBaseUrl() . 'index.php/api/soap/?wsdl');
            $session = $client->login('seller', 'seller');
	
			
            $result = $client->call($session, 'customer.create', array(array('email' => $_POST['email'], 'firstname' => $_POST['first-name'], 'lastname' => $_POST['last-name'], 'password' => $_POST['password'], 'website_id' => Mage::app()->getWebsite()->getId(), 'store_id' => 1, 'group_id' => 4)));
            $result = $client->call(
                    $session, 'customer_address.create', array('customerId' => $result, 'addressdata' => array('firstname' => $_POST['first-name'], 'lastname' => $_POST['last-name'], 'street' => array($_POST['street'], $_POST['street']), 'city' => $_POST['city'], 'country_id' => 'IN', 'region' => '', 'region_id' => 3, 'postcode' => '411045', 'company' => $_POST['company'], 'taxvat' => $_POST['vat_id'], 'telephone' => $_POST['telephone'], 'is_default_billing' => TRUE, 'is_default_shipping' => TRUE)));
					
				$newarray =  array('customerId' => $result, 'email' => $_POST['email'], 'customername' => $_POST['first-name'].' '.$_POST['last-name'], 'street' => $_POST['street'], 'city' => $_POST['city'], 'postcode' => '411045');
			
				Mage::getSingleton('core/session')->setMyValue($newarray);
				
				$customer = Mage::getModel("customer/customer");
				$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
				$customer->loadByEmail($_POST['email']);
				$session = Mage::getSingleton('customer/session');
				$session->login($_POST['email'], $_POST['password']);

            $this->_redirect('seller?action=sellerPaypal&custId=' . $_POST['email']);
            //$this->_redirect('subcription_page/?___store=default' );
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'seller.log');
            //$this->_redirect('seller?action=sellerRegister');
            $this->_redirect('seller?sellerLogin&wrongRegister=1');
        }
    }

   public function emailAction() {
       $arrProduct = explode('productf=', $_SERVER['HTTP_REFERER']);

        $buyerEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail(); //Buyer email, from
        $buyerName = Mage::getSingleton('customer/session')->getCustomer()->getName(); //Buyer email, from
        
        
               
        $storeId = Mage::app()->getStore()->getId();
       

        if (isset($_REQUEST['product']) && $_REQUEST['product']) {
            $intProductID = $_REQUEST['product'];
        }
        if (!$intProductID)
            $intProductID = $arrProduct[1];

        $_product = Mage::getModel('catalog/product')->load($intProductID);
        $strProductName = $_product->getName();
         $strProductPartno = $_product->getSku();
            
        $manufacutNumber = $_product->getManufacturerNumber();
        $location = $_product->getlocation();
        $email = $_product->getSellerEmail();
      
	$customer = Mage::getModel("customer/customer");
	 $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
	$customer->loadByEmail($email); 

	$strCustomerAdd = $customer->getDefaultBilling(); //Buyer email, from
	$strTelephone = '';
	$strCompanyName ='';
       	if ($strCustomerAdd) {
            $strAdd = Mage::getModel('customer/address')->load($strCustomerAdd);
              $strCompanyName = $strAdd->getCompany();   
             $strTelephone = $strAdd->getTelephone(); 
        } 
         $stockItem = Mage::getModel('cataloginventory/stock_item')
                ->loadByProduct($_product->getId());

        $intProductQnty = intval($stockItem->getQty());
        

        $mail = new Zend_Mail();
        $mailSubject = 'Request To get Quatation From SCS Nimkar Consultancy';
        
        $name = '';
             
         
                
             //  "product manaufacturer is " . $manufacutName);
                //$mail->setBodyText($strBody);
         
        $mail->setBodyText('              
        Manufacturer Name = ' .$strProductName . 
                 
        //'Supplir Part No ='.$strProductPartno.
        ',                                                                                                               
        Manufacturer Part No = ' .  $manufacutNumber .
        //'Seller Name ='.$strcompany.
        ',                                                                    
        Location = ' . $location .
        ',                                                                   
        Quantity = ' . $intProductQnty  ); 
       // ',
       // Telephone = ' . $strTelephone . 
       // ',
       // Company = ' .$strCompanyName  );
        
        $mail->setFrom($buyerEmail);
        $mail->addTo($email);
        $mail->addCc('narayan.jadhav1988@gmail.com');
        $mail->setSubject($mailSubject);
        try {
            $mail->send();
            $this->_redirect('seller?action=sellerEmailSent');
        } catch (Exception $e) {
            $this->_redirect('seller?action=sellerEmailSent');
        }
    }

}

<?php
	
// 9272767481    
	// Add RSS links to <head> section
//	automatic_feed_links();

include ("include/cpt.php");
include ("include/meta-boxes.php");
include('theme_option.php');

add_action( 'init', 'create_book_taxonomies', 0 );
function create_book_taxonomies(){ 
$labels = array(
        'name'                       => _x( 'Package Category', 'taxonomy general name' ),
        'singular_name'              => _x( 'Package Category', 'taxonomy singular name' ),
        'search_items'               => __( 'Search Package Category' ),
        'popular_items'              => __( 'Popular Package Categories' ),
        'all_items'                  => __( 'All Package Categories' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Package Category' ),
        'update_item'                => __( 'Update Package Category' ),
        'add_new_item'               => __( 'Add New Package Category' ),
        'new_item_name'              => __( 'New Package Category' ),
        'separate_items_with_commas' => __( 'Separate Package Categories with commas' ),
        'add_or_remove_items'        => __( 'Add or remove Package Category' ),
        'choose_from_most_used'      => __( 'Choose from the most used writers' ),
        'not_found'                  => __( 'No Package Category found.' ),
        'menu_name'                  => __( 'Package Categories' ),
    );

    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'package-category' ),
    );

    register_taxonomy( 'Package Category', 'packages', $args );

}









add_action("template_redirect", 'ProjectTheme_template_redirect');

function projectTheme_template_redirect()
{
    //echo'aaaaaaaaaa';exit;
        global $wp;
        global $wp_query, $post, $wp_rewrite;

        if(isset($_GET['my_upload_of_project_files_proj']))
        {
            //echo '2';
            //echo '<pre>';print_r($_GET);exit;
            get_template_part( 'include/uploady5');
            die();  
        }
}

function ProjectTheme_get_auto_draft($uid)
    {

        global $wpdb;   
        $querystr = "
            SELECT distinct wposts.* 
            FROM $wpdb->posts wposts where 
            wposts.post_author = '$uid' AND wposts.post_status = 'auto-draft' 
            AND wposts.post_type = 'project' 
            ORDER BY wposts.ID DESC LIMIT 1 ";
                    
        $row = $wpdb->get_results($querystr, OBJECT);
        if(count($row) > 0)
        {
            $row = $row[0];
            return $row->ID;
        }
        
        return ProjectTheme_create_auto_draft($uid);    
}
function ProjectTheme_using_permalinks()
{
    global $wp_rewrite;
    if($wp_rewrite->using_permalinks()) return true; 
    else return false;  
}
function ProjectTheme_create_auto_draft($uid)
{
    //echo 'aaaaaaaa';exit;
        $my_post = array();
        $my_post['post_title']      = 'Auto Draft';
        $my_post['post_type']       = 'circuits';
        $my_post['post_status']     = 'auto-draft';
        $my_post['post_author']     = $uid;
        $pid = wp_insert_post( $my_post, true );
        
      // echo $pid;exit;
        
        return $pid;
            
}
function theme_name_scripts() {
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri(). '/css/bootstrap.min.css' );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js' );
}


add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }

	
register_nav_menus( array(

 'primary' => __( 'Primary Navigation', 'theme name here' ),
 ) );	



add_filter( 'manage_edit-circuits_columns', 'my_edit_circuits_columns' ) ;

function my_edit_circuits_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Circuits Name' ),
        'can_course' => __( 'User Uploaded File' ),
        'can_admin' => __( 'Admin Uploaded File' ),
        'date' => __( 'Date' )
    );

    return $columns;
}

add_action( 'manage_circuits_posts_custom_column', 'my_manage_circuits_columns', 10, 2 );

function my_manage_circuits_columns( $column, $post_id ) { 

    global $post;
    switch( $column ) { 
        case 'can_course' :
            /* Get the post meta. */


            $attachments = get_children( array(
                'post_parent'    => $post_id,
                'post_type'      => 'attachment',
                'numberposts'    => -1, // show all -1
                'post_status'    => 'inherit',
                'order'          => 'ASC',
                'orderby'        => 'menu_order ASC'
                ) );
           
            //echo $fullsize_path ;exit;
            foreach ( $attachments as $attachment ) {
                 $image = wp_get_attachment_url($attachment->ID);
                 $aa = get_post_meta ( $post_id, '_xxxx_attached_image', true );
                 if($aa == '') { 
                //$fullsize_path = get_attached_file( $attachment_id ); ?>
                 <a href="<?php echo $image; ?>">download file ( <?php echo $attachment->post_title ?> ) <a> <br>
                <?php }else{
                    echo __( 'Empty' );
                }}
    
            /* If no duration is found, output a default message. */
            if ( empty( $image ) )
                echo __( 'Empty' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( $can_course );
            break;
            case 'can_admin' :
                $atts = get_children( array(
                'post_parent'    => $post_id,
                'post_type'      => 'attachment',
                'numberposts'    => -1, // show all -1
                'post_status'    => 'inherit',
                'order'          => 'ASC',
                'orderby'        => 'menu_order ASC'
                ) );
           
            //echo $fullsize_path ;exit;
            foreach ( $atts as $att ) {
                 $file = wp_get_attachment_url($att->ID);
                 $aaa = get_post_meta ( $post_id, '_xxxx_attached_image', true );
                 if($aaa != '') { 
                //$fullsize_path = get_attached_file( $attachment_id ); ?>
                 <a href="<?php echo $file; ?>">download file <a> <br>
            <?php }else{
                echo __( 'Empty' );
            }
        }
            break;
    }
}





add_filter( 'manage_edit-candidates_columns', 'my_edit_candidates_columns' ) ;

function my_edit_candidates_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Candidate Name' ),
        'can_course' => __( 'Course Name' ),
        'can_course_fees' => __( 'Course Fees' ),
        'payment_mode' => __( 'Fees Mode' ),
        'can_batch' => __( 'Batch' ),
        'can_email' => __( 'Email' ),
        'date' => __( 'Date' )
    );

    return $columns;
}

add_action( 'manage_candidates_posts_custom_column', 'my_manage_movie_columns', 10, 2 );

function my_manage_movie_columns( $column, $post_id ) { 

    global $post;
    switch( $column ) { 
        case 'can_course' :
            /* Get the post meta. */
            $can_course = get_post_meta( $post_id, 'can_course', true );

            /* If no duration is found, output a default message. */
            if ( empty( $can_course ) )
                echo __( 'Unknown' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( $can_course );

            break;
        case 'can_batch' :
            /* Get the post meta. */
            $can_course = get_post_meta( $post_id, 'can_batch', true );

            /* If no duration is found, output a default message. */
            if ( empty( $can_course ) )
                echo __( 'Unknown' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( $can_course );

            break;
        case 'can_course_fees' :
            /* Get the post meta. */
            $can_course_fees = get_post_meta( $post_id, 'can_course_fees', true );

            /* If no duration is found, output a default message. */
            if ( empty( $can_course_fees ) )
                echo __( 'Unknown' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( __( '%s Rs' ), $can_course_fees );

            break;

        case 'payment_mode' :
            /* Get the post meta. */
            $payment_mode = get_post_meta( $post_id, 'payment_mode', true );

            /* If no duration is found, output a default message. */
            if ( empty( $payment_mode ) )
                echo __( 'Unknown' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( $payment_mode );

            break;

        case 'can_email' :
            /* Get the post meta. */
            $can_email = get_post_meta( $post_id, 'can_email', true );

            /* If no duration is found, output a default message. */
            if ( empty( $can_email ) )
                echo __( 'Unknown' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf( $can_email );

            break;
    }
}

//ajax call for login
add_action( 'wp_ajax_ajax_login', 'ajax_login' );
add_action( 'wp_ajax_nopriv_ajax_login', 'ajax_login');

function ajax_login(){
    //$exploded_string = explode("&",$_POST['form_data']);
    //$username = explode('=',$exploded_string[0]);
    //$usr_nm = $username[1]; 
    //$pwd = explode('=',$exploded_string[1]);
    //$password = $pwd[1];
    //echo 'sujeet';exit;

    $user_name = $_POST['user_name'];
    $password = $_POST['password'];
    $creds = array();
    $creds['user_login'] = $user_name;
    $creds['user_password'] = $password;
    $creds['remember'] = true;

    $user = wp_signon( $creds, true );
    //echo '<pre>';print_r($user);exit;
    //echo $stripped_tag = strip_tags($user->get_error_message());
    if ( is_wp_error($user) ){
        $stripped_tag = strip_tags($user->get_error_message());
        $msg = str_replace('Lost your password?', '', $stripped_tag);   
        echo $msg;
    }
    else{
        echo 'success';
        wp_set_auth_cookie( $user->ID,true);
    }
    exit(0);
}
 
add_action('wp_footer','forgot_password');
//forgot password
function forgot_password(){
    echo '<div class="modal fade" id="forget_pwd_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Forgot Password</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" id="forgotPass">
                                <p>
                                    <input class="form-control" type="email" id="forgot_pwd_email" name="email" placeholder="Enter Email Address here"/>
                                </p>
                                <p>
                                    <input type="submit" class="btn btn-default" id="get_pwd" value="Get New Password" />
                                </p>
                                <p class="success_msg"></p>
                                <p class="error_msg"></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>'; ?>
            <script type="text/javascript">

var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}

//Ajax call for signup
add_action( 'wp_ajax_ajax_signup', 'ajax_signup' );
add_action( 'wp_ajax_nopriv_ajax_signup', 'ajax_signup');

function ajax_signup(){
    //echo '<pre>';print_r($_POST);exit;
    $acc_type = $_POST['acc_type'];
    $terms = $_POST['terms'];
    $cpm_name = $_POST['cpm_name'];
    $email = $_POST['email'];
    $name = $_POST['name'];
    $role = $_POST['role'];
    $nameArr = explode(" ",$_POST['name']);
    $firstname = $nameArr[0];
    if($nameArr[1]){
        $lastname = $nameArr[1];
    }else {
        $lastname = '';
    }
    
    $contact_number = $_POST['mobile'];
    $password = $_POST['password'];
    $user_id = username_exists( $email);

    if ( !$user_id and email_exists($email) == false ) {
        $user_id = wp_create_user( $email, $password, $email );
        $args = array(
            'ID' => $user_id,
            'nickname' => $firstname,
            'display_name' => $firstname,
            'role' => $role
        );
        wp_update_user( $args );
        update_user_meta($user_id,'first_name',$firstname);
        update_user_meta($user_id,'terms&conditions',$terms);
        update_user_meta($user_id,'cpm_name',$cpm_name);
        update_user_meta($user_id,'acc_type',$acc_type);
        update_user_meta($user_id,'contact_number',$contact_number);
        update_user_meta($user_id,'last_name',$lastname);
        update_user_meta($user_id,'activated','no');
        $creds['user_login'] = $email;
        $creds['user_password'] = $password;
        $creds['remember'] = true;
        //$current_user = wp_signon( $creds, true );    

    // sending accounr/email confirmation email while siging up 

    //$current_user = wp_get_current_user();
    //$code = sha1( $current_user->ID . time() );
    $code = sha1( $user_id . time() ); 
    $activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_permalink(5422));
    add_user_meta( $user_id, 'has_to_be_activated', $code, true );
    $message = 'Dear User,<br /><br />
    If you requested this verification, please click on the following URL to confirm your account.<br />
    Here is the activation link: ' . $activation_link;
    /*add_filter( 'wp_mail_content_type', 'set_content_type' );
    add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
    add_filter( 'wp_mail_from', 'change_mail_from');*/
    
    wp_mail( $email, 'Activate your account', $message );
        echo 'success';
    } else {
        echo 'User already exists with this email id';
    }

    exit(0);
}
add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page( 'User Balance', 'User Balance', 'manage_options', 'userbalance', 'my_custom_menu_page', plugins_url( 'myplugin/images/icon.png' ), 6 ); 

}

function my_custom_menu_page() {
    
    echo '<div class="wrap">';
    echo '<div class="icon32" id="icon-options-general-bal"><br/></div>';   
    echo '<h2 class="my_title_class_sitemile">Set User Balances</h2>';
    ?>
        <script>
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
     jQuery(document).ready(function() {
  
    jQuery('.update_btn*').click(function() {
    
    var id = jQuery(this).attr('alt');
    var increase_credits = jQuery('#increase_credits' + id).val();
    var decrease_credits = jQuery('#decrease_credits' + id).val();


    jQuery.ajax({
            type: 'POST',
            context: this,
            url: ajaxurl,
            data: {action: 'update_user_balance','crds':'1','uid':id,'increase_credits':increase_credits,'decrease_credits':decrease_credits},
            success: function(msg){
                jQuery("#money" + id).html(msg);
                jQuery('#increase_credits' + id).val("");
                jQuery('#decrease_credits' + id).val("");  
            }
        });
    
    }); 
}); 
    </script>
    <div id="usual2" class="usual"> 
    <?php
    $rows_per_page = 10;
    if(isset($_GET['pj'])) $pageno = $_GET['pj'];
    else $pageno = 1;
    global $wpdb;
    $s1 = "select ID from ".$wpdb->users." order by user_login asc ";
    $s = "select * from ".$wpdb->users." order by user_login asc ";
    $limit = 'LIMIT ' .($pageno - 1) * $rows_per_page .',' .$rows_per_page;
    $r = $wpdb->get_results($s1); $nr = count($r);
    $lastpage      = ceil($nr/$rows_per_page);
    $r = $wpdb->get_results($s.$limit);
    if($nr > 0) 
    {
    ?>
    <table class="widefat post fixed" cellspacing="0">
    <thead>
    <tr>
    <th width="15%">Username</th>
    <th width="20%">Email</th>
    <th width="20%">Date Registered</th>
    <th width="13%" >Credits Balance</th>
    <th >Options</th>
    </tr>
    </thead>
    <tbody>
    <?php 
    foreach($r as $row)
    {
        $user = get_userdata($row->ID);
        echo '<tr style="">';   
        echo '<th>'.$user->user_login.'</th>';
        echo '<th>'.$row->user_email .'</th>';
        echo '<th>'.$row->user_registered .'</th>';
        echo '<th class="'.$cl.'"><span id="money'.$row->ID.'">'.$sign. projectTheme_get_credits($row->ID) .'</span></th>';
        echo '<th>'; 
        ?>
        Increase Credits: <input type="text" size="4" id="increase_credits<?php echo $row->ID; ?>" rel="<?php echo $row->ID; ?>" /><br/>
        Decrease Credits: <input type="text" size="4" id="decrease_credits<?php echo $row->ID; ?>" rel="<?php echo $row->ID; ?>" /><br/>
        <input type="button" value="Update" class="update_btn" alt="<?php echo $row->ID; ?>" />       
        <?php
        echo '</th>';
        echo '</tr>';
    }
    ?>
    </tbody>   
    </table>
    <?php 
    for($i=1;$i<=$lastpage;$i++)
        {
            if($pageno == $i) echo $i." | ";
            else            
            echo '<a href="'.get_admin_url().'admin.php?page=userbalance&pj='.$i.'"
            >'.$i.'</a> | ';        
        }    
    }



    /*********************** Plan Setting **************************/
        
    /*********************** End Of Plan Setting *******************/



}

function ProjectTheme_get_credits($uid)
{
    $c = get_user_meta($uid,'credits',true);
    if(empty($c))
    {
        update_user_meta($uid,'credits',"0");   
        return 0;
    }
    
    return $c;
}

function projectTheme_update_credits($uid,$am)
{
   
    update_user_meta($uid,'credits',$am);   

}

//Ajax call for signup
add_action( 'wp_ajax_update_user_balance', 'update_user_balance' );
add_action( 'wp_ajax_nopriv_update_user_balance', 'update_user_balance');

function update_user_balance(){

    if(isset($_POST['crds']))
    {   
        $uid = $_POST['uid'];
        if(!empty($_POST['increase_credits']))
        {
            if($_POST['increase_credits'] > 0)
            if(is_numeric($_POST['increase_credits']))
            {
                $cr = projectTheme_get_credits($uid);
                projectTheme_update_credits($uid, $cr + $_POST['increase_credits']);           
            }
        }
        else
        {
            if($_POST['decrease_credits'] > 0)
            if(is_numeric($_POST['decrease_credits']))
            {
                $cr = projectTheme_get_credits($uid);
                projectTheme_update_credits($uid, $cr - $_POST['decrease_credits']);
            }
        
        }   
        echo (projectTheme_get_credits($uid)) ;
        exit;
    } 

}

function get_earn_pt_data($user_id) {
    global $wpdb;
    $like=0; $share=0; $comment=0; $view=0; $total=0;
    $table = $wpdb->prefix . 'usermeta';
    $records = $wpdb->get_results( "SELECT * FROM  $table Where user_id=$user_id and meta_key='credits'");
    $credits = $records[0]->meta_value;
    $earning_data = array('credits' => $credits);
    return $earning_data;
}
add_action( 'post_edit_form_tag' , 'post_edit_form_tag' );

function post_edit_form_tag( ) {
    echo ' enctype="multipart/form-data"';
}

function xxxx_render_image_attachment_box($post) {
    $existing_image_id = get_post_meta($post->ID,'_xxxx_attached_image', true);
    if(is_numeric($existing_image_id)) {
        echo '<div>';
            $arr_existing_image = wp_get_attachment_image_src($existing_image_id, 'large');
            $existing_image_url = $arr_existing_image[0];
            echo '<img src="' . $existing_image_url . '" />';
        echo '</div>';
    }
    if($existing_image) {
        echo '<div>Attached Image ID: ' . $existing_image . '</div>';
    } 
    echo 'Upload an File: <input type="file" name="xxxx_image" id="xxxx_image" />';
    $status_message = get_post_meta($post->ID,'_xxxx_attached_image_upload_feedback', true);
    // Show an error message if there is one
    if($status_message) {
        echo '<div class="upload_status_message">';
            echo $status_message;
        echo '</div>';
    }
    echo '<input type="hidden" name="xxxx_manual_save_flag" value="true" />';
}



function xxxx_setup_meta_boxes() {
    // Add the box to a particular custom content type page
    add_meta_box('xxxx_image_box', 'Upload File', 'xxxx_render_image_attachment_box', 'circuits', 'normal', 'high');
}
add_action('admin_init','xxxx_setup_meta_boxes');

function xxxx_update_post($post_id, $post) {
//echo $download_link = site_url().'/download/?post_id='.$post_id;
    //echo '<pre>';print_r($post);exit;
    $post_type = $post->post_type;
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    if($post_id && isset($_POST['xxxx_manual_save_flag'])) { 
        switch($post_type) {
        case 'circuits':
                if(isset($_FILES['xxxx_image']) && ($_FILES['xxxx_image']['size'] > 0)) {
                    $arr_file_type = wp_check_filetype(basename($_FILES['xxxx_image']['name']));
                    $uploaded_file_type = $arr_file_type['type'];
                        $upload_overrides = array( 'test_form' => false ); 
                        $uploaded_file = wp_handle_upload($_FILES['xxxx_image'], $upload_overrides);
                        if(isset($uploaded_file['file'])) {
                            $file_name_and_location = $uploaded_file['file'];
                            $file_title_for_media_library = 'your title here';
                            $attachment = array(
                                'post_mime_type' => $uploaded_file_type,
                                'post_title' => 'Uploaded image ' . addslashes($file_title_for_media_library),
                                'post_content' => '',
                                'post_status' => 'inherit'
                            );
                            $attach_id = wp_insert_attachment( $attachment, $file_name_and_location, $post_id );
                            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                            $attach_data = wp_generate_attachment_metadata( $attach_id, $file_name_and_location );
                            wp_update_attachment_metadata($attach_id,  $attach_data);
                            $existing_uploaded_image = (int) get_post_meta($post_id,'_xxxx_attached_image', true);
                            if(is_numeric($existing_uploaded_image)) {
                                wp_delete_attachment($existing_uploaded_image);
                            }
                            update_post_meta($post_id,'_xxxx_attached_image',$attach_id);
                            $password = rand();
                            update_post_meta($post_id,'_xxxx_attached_file_password',$password);
                            update_post_meta($post_id,'_xxxx_attached_file_download','No');

                            $admin_email = get_option( 'admin_email' );
                            $message = 'Dear Sir,<br /><br />
                            Password for document file : ' . $password. '<br /><br />
                            Circuit ID : '.$post_id.'<br/><br/>
                            Circuit Name : '.$post->post_title.'<br/><br/>
                            Date : '.$post->post_date;

                            wp_mail( $admin_email, 'Activate your account', $message );


                            $download_link = site_url().'/download/?post_id='.$post_id;
                            $message = 'Dear User,<br /><br />
                            Please click on the following URL to download the file.<br />
                            ' . $download_link;
                            wp_mail( $admin_email, 'Activate your account', $message );



                            $upload_feedback = false;
                        } else { 
                            $upload_feedback = 'There was a problem with your upload.';
                            update_post_meta($post_id,'_xxxx_attached_image',$attach_id);

                        }
                } else { // No file was passed
                    $upload_feedback = false;
            }
                update_post_meta($post_id,'_xxxx_attached_image_upload_feedback',$upload_feedback);
            break;
            default:
        } // End switch
    return;
} // End if manual save flag
    return;
}
add_action('save_post','xxxx_update_post',1,2);
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   
	<?php } ?>

	<title>
		  <?php wp_title(''); ?>
          <?php if(wp_title('', false)) { echo ' :'; } ?>
          <?php bloginfo('name'); ?>
	</title>
	
	<!--Links-->
	 <link rel="shortcut icon" href="<?php bloginfo ('template_url'); ?>/images/favicon.png" type="image/x-icon" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    
	 <script type="text/javascript" src="<?php bloginfo ('template_url'); ?>/js/bootstrap.min.js"></script>

	
	<link href="<?php bloginfo ('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	
	
	
	
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>
<div class="strip"></div>
	<div id="wrapper">
	<div id="container"><!--Main Wrapper-->
 
	<div id="header">
	<div id="head_in">

		<!-- <a href="http://scsnimkar.com/"><img src="<?php bloginfo ('template_url'); ?>/images/spa.png" height="82" width="100%"></a>
		<div class="tagline">Sejal Consultancy Services</div> -->
		 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="http://scsnimkar.com/"><img src="<?php bloginfo ('template_url'); ?>/images/spa.png" height="82" width="100%"></a>
		</div>
		<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
			<div class="tagline">Sejal Consultancy Services</div>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="margin-top: 30px;text-align: right;">
			<?php  if ( is_user_logged_in() ) : 
				global $current_user;
						get_currentuserinfo();

                $username = $current_user->user_login;
                $length = strlen( $username );
                if($length > 14) {
                    $string = substr($username , 0, 14);
                    $string .= '...';   
                }
                   else{
                        $string =  $username;
                   }
            ?>
				<a href="#"><?php echo 'WELCOME '.$string; ?></a> |
				<a href="<?php echo wp_logout_url(home_url());?>" >Logout</a>
			<?php else: ?>
				<a id="login" href="#">Sign in</a> | <a id="signup-main" href="#" >Sign Up</a>
			<?php endif ?>
		</div> 

		<div id="loginpanel" <?php if (get_option('users_can_register')) { ?>style="display:none;"<?php }else{ ?>style="display:none;right:0;"<?php } ?>>
			<div class="content">
			<div id="box-shadow" class="clearfix">
			<div class="account-box" id="sign-tool"  >             
			<h3 class="header_text"> WELCOME BACK!</h3>             
			<?php do_action( 'appthemes_notices' ); ?>
			<div>
			<p class="error_msg"></p>
			<p class="success_msg"></p>
			</div>
			<form  action="<?php echo wp_login_url(); ?>" method="post" class="loginForm" id="loginForm">
			<p class="form-group">
			<input type="text" name="log" class="form-control" tabindex="1" id="login_username" required value="<?php if (isset($_POST['login_username'])) echo esc_attr( $_POST['login_username'] ); ?>" />
			</p>
			<p class="form-group">
			<input type="password" name="pwd" class="form-control" tabindex="2" id="login_password" value="" required/>
			</p>   
			<button tabindex="4" type="submit" class="btn btn-lg btn-block" id="sign-btn" name="login" value="login">Login</button>
			<input type="hidden" name="testcookie" value="1" />
			</form>
			<a class="forgot_action" href="#" title="Password Lost and Found">Forgot password?</a>

			<?php //echo appthemes_get_password_recovery_url(); ?>

			</div>           
			

			</div>
			</div>
		</div>



		<div id="signup-form" style="display:none;right:0;">
			<div class="content">
			<div id="box-shadow" class="clearfix">
			<div class="account-box" id="sign-tool"  >             
			<h3 class="header_text"> WELCOME BACK!</h3>             
			<div>
			<p class="error_msg"></p>
			<p class="success_msg"></p>
			</div>
			<form  action="<?php echo wp_login_url(); ?>" method="post" class="loginForm" id="signupForm">
			
			<p class="form-group">
			<select required class="form-control" name="role" id="role">
				<option value="">Select Account Type</option>
				<option value="subscriber">Individual</option>
				<option value="contributor">Company</option>
			</select>
			</p>
				
			<p class="form-group">
				<input required type="hidden" name="acc_type" id="acc_type" value="" />
			<input type="text" name="name" placeholder="Name" class="form-control" tabindex="1" id="name" required value="<?php if (isset($_POST['name'])) echo esc_attr( $_POST['name'] ); ?>" />
			</p>

			<p class="form-group">
			<input type="text" name="cpm_name" placeholder="Company Name" class="form-control" tabindex="2" id="cpm_name" required value="<?php if (isset($_POST['cpm_name'])) echo esc_attr( $_POST['cpm_name'] ); ?>" />
			</p>

			<p class="form-group">
			<input type="number" placeholder="Mobile Number" maxlength="10" name="mobile" class="form-control" tabindex="3" id="mobile" required value="<?php if (isset($_POST['mobile'])) echo esc_attr( $_POST['mobile'] ); ?>" />
			</p>

			<p class="form-group">
			<input type="email" name="email" placeholder="Email" class="form-control" tabindex="4" id="email" required value="<?php if (isset($_POST['email'])) echo esc_attr( $_POST['email'] ); ?>" />
			</p>

						
			<p class="form-group">
			<input type="password" placeholder="Password" name="password" class="form-control" tabindex="6" id="password" value="" required/>
			</p> 

			<p class="form-group">
			<input type="password" placeholder="Confirm Password" name="cfm_password" class="form-control" tabindex="7" id="cfm_password" value="" required/>
			</p> 
			<p class="form-group">
			<label class="checkbox-inline"><input type="checkbox" name="terms" id="terms" value="1" required/>Terms & Conditions </label>
			</p> 

			<button tabindex="8" type="submit" class="btn btn-lg btn-block" id="sign-btn" name="signup" >Sign Up</button>
			</form>
			

			<?php //echo appthemes_get_password_recovery_url(); ?>

			</div>           
			   

			</div>
			</div>
		</div>


	</div>	

<div id="menubar"><!--#menubar-->
<?php if ( is_user_logged_in() ) { ?>
<?php 
 $curauth = wp_get_current_user();
 //echo '<pre>';print_r($curauth->roles[0]);exit;
 if($curauth->roles[0] == 'administrator' || $curauth->roles[0] == 'subscriber') {
 	wp_nav_menu( array('menu_id' => 'jetmenu_m', 'menu_class' => 'jetmenua bluea' , 'fallback_cb' => '', 'menu' => 'secondary-menu', 'container' => false ) ); 
 }elseif($curauth->roles[0] == 'contributor') {
 	wp_nav_menu( array('menu_id' => 'jetmenu_m', 'menu_class' => 'jetmenua bluea' , 'fallback_cb' => '', 'menu' => 'company-menu', 'container' => false ) ); 
 }
?>

<?php } else { ?>
<?php wp_nav_menu( array('menu_id' => 'jetmenu_m', 'menu_class' => 'jetmenua bluea' , 'fallback_cb' => '', 'menu' => 'scs', 'container' => false ) ); ?>
<?php } ?>
</div><!-- #menubar -->
		
</div>
<script>
$("#signupForm").validate({
	rules: {
		password: { 
		required: true,
		} , 
		cfm_password: { 
		equalTo: "#password",
		}
	}
});
</script>
<script type="text/javascript">

var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

jQuery(document).ready(function() {
	
	jQuery("#login").click(function () {
		
      jQuery("div#loginpanel").slideToggle("slow");
      jQuery("div#signup-form").hide();
      
    });

    jQuery("#signup-main").click(function () {
      jQuery("div#signup-form").slideToggle("slow");
       jQuery("div#loginpanel").hide();

    });
		
});

$(function() { 
  $("#role").on("change",function() { 
    //$("#inp").val($(this).val());   company-parent
   var a = $(this).val();
   if(a == 'subscriber')
   {
   		var s = 'individual-parent';
   		document.getElementById("acc_type").value = s;
   }
   if(a == 'contributor')
   {
   		var s = 'company-parent';
   		document.getElementById("acc_type").value = s;
   }
  }); 
}); 

 jQuery(document).on("submit", '#loginForm',function(e){
        e.preventDefault();
        jQuery('.error_msg').text('Loading...').fadeIn();
        var user_name = jQuery('#login_username').val();
        var password = jQuery('#login_password').val();
        jQuery.ajax({
            type: 'POST',
            context: this,
            url: ajaxurl,
            data: {action: 'ajax_login','user_name':user_name,'password':password},
            success:function(data){
                jQuery('.error_msg').fadeOut();
                if(jQuery.trim(data) == jQuery.trim('success')){
                   jQuery('.success_msg').text('Login successful. Redirecting....').fadeIn();  
                     location.reload();   
                }
                else{
                    jQuery('.error_msg').text(data).fadeIn();   
                }
            }
        });  
    });

 jQuery(document).on("submit", '#signupForm',function(e){
        e.preventDefault();
        jQuery('.error_msg').text('Loading...').fadeIn();
        var acc_type = jQuery('#acc_type').val();
        var role = jQuery('#role').val();
        var name = jQuery('#name').val();
        var cpm_name = jQuery('#cpm_name').val();
        var mobile = jQuery('#mobile').val();
        var email = jQuery('#email').val();
        var password = jQuery('#password').val();
        var cfm_password = jQuery('#cfm_password').val();
        var terms = jQuery('#terms').val();
        alert(terms);
        
        alert(cpm_name);
        jQuery.ajax({
            type: 'POST',
            context: this,
            url: ajaxurl,
            data: {action: 'ajax_signup','terms':terms,'role':role,'acc_type':acc_type,'name':name,'cpm_name':cpm_name,'mobile':mobile,'email':email,'password':password},
            success:function(data){
                jQuery('.error_msg').fadeOut();
                if(jQuery.trim(data) == jQuery.trim('success')){
                   jQuery('.success_msg').text('Please check email for account activation').fadeIn();  
                     location.reload();   
                }
                else{
                    jQuery('.error_msg').text(data).fadeIn();   
                }
            }
        });  
    });

</script>

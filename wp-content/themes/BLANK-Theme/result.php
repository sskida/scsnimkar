<?php 
/*
Template Name: Results
*/
?>

<?php get_header(); ?>

<div class="row">
      <section id="courses-list">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            <?php if( $_GET['res']== 0 && isset($_GET['msg']) ) { ?>
            <div id="message" class="updated">
                <p><strong><?php _e('Payment has been received.') ?></strong></p>
            </div>
        <?php } elseif( ($_GET['res']== 1 || $_GET['res']== 2 ) && isset($_GET['msg']) ) { ?>
            <div id="err-message" class="updated">
                <p><strong><?php echo $_GET['msg']; ?></strong></p>
            </div>
        <?php }else { ?>
            <div id="err-message" class="updated">
                <p><strong><?php _e('Something worng.') ?></strong></p>
            </div>
        <?php } ?>
        	
            <a style="text-align:center;margin:10px;" class="btn btn-danger btn-lg" href="<?php echo site_url();?>">Return to home page</a>
	    </div>
	</section>
</div>
    

	
<?php //get_sidebar(); ?>

<?php get_footer(); ?>
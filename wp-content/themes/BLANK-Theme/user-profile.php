<?php
/**
 * Template Name: User Profile
 *
 * Allow users to update their profiles from Frontend.
 *
 */

/* Get user info. */
global $current_user, $wp_roles;
//get_currentuserinfo(); //deprecated since 3.1

/* Load the registration file. */
//require_once( ABSPATH . WPINC . '/registration.php' ); //deprecated since 3.1
$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    //echo '<pre>';print_r($_POST);exit;
    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
    }

    /* Update user information. */
   
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
            $error[] = __('This email is already used by another user.  try a different one.', 'profile');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first_name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first_name'] ) );
    if ( !empty( $_POST['last_name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last_name'] ) );
    
     if ( !empty( $_POST['contact_number'] ) )
        update_user_meta( $current_user->ID, 'contact_number', esc_attr( $_POST['contact_number'] ) );
     

    /* Redirect so the page will show updated info.*/
  /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( get_permalink() );
        exit;
    }
}
?>


<?php get_header(); ?>
<div id="primary" class="site-content">
  <div id="content" role="main">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>">
        <div class="entry-content entry">
            <?php the_content(); ?>
            <?php if ( !is_user_logged_in() ) : ?>
                    <p class="warning">
                        <?php _e('You must be logged in to edit your profile.', 'profile'); ?>
                    </p><!-- .warning -->
            <?php else : ?>
                <?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var _custom_media = true,
        _orig_send_attachment = wp.media.editor.send.attachment;
        $('#_unique_name_button').click(function(e) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(this);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment){
                if ( _custom_media ) {
                    $(".fb-image-profile #prof_img").show();
                    $('.fb-image-profile .avatar').hide();
                    $("#prof_img").attr('src',attachment.url);
                    $('#prof_img_input').val(attachment.url);
                } else {
                    return _orig_send_attachment.apply( this, [props, attachment] );
                };
            }
     
            wp.media.editor.open(button);
            return false;
    });

     $('#update_image').click(function(e) {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var id = button.attr('id').replace('_button', '');
        _custom_media = true;
        wp.media.editor.send.attachment = function(props, attachment){
            if ( _custom_media ) {
                 $("#cover_img").attr('src',attachment.url);
                 $('#cover_img_input').val(attachment.url);
            } else {
                return _orig_send_attachment.apply( this, [props, attachment] );
            };
        }
 
        wp.media.editor.open(button);
        return false;
    });
 
    $('.add_media').on('click', function(){
        _custom_media = false;
    });
});



</script>
    

               <h1 style="text-align:center" >Update Profile</h1>
                <form id="edit_profile" class="form-horizontal" role="form"method="post" id="adduser" action="<?php the_permalink(); ?>">
    
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label"><?php _e('First Name', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="first_name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last-name" class="col-sm-2 control-label"><?php _e('Last Name', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="last_name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label"><?php _e('Mobile *', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="contact_number" type="number" id="contact_number" value="<?php the_author_meta( 'contact_number', $current_user->ID ); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label"><?php _e('E-mail *', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="pass1" class="col-sm-2 control-label"><?php _e('Password *', 'profile'); ?> </label>
                        <div class="col-sm-7">
                            <input class="form-control" name="pass1" type="password" id="pass1" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass2" class="col-sm-2 control-label"><?php _e('Repeat Password *', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="pass2" type="password" id="pass2" />
                        </div>
                    </div>
                    
                    

                    <?php 
                        //action hook for plugin and extra fields
                        do_action('edit_user_profile',$current_user); 
                    ?>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        <?php echo $referer; ?>
                        <input name="updateuser"  type="submit" id="updateuser" class="btn" value="<?php _e('Update', 'profile'); ?>" />
                        <?php wp_nonce_field( 'update-user' ) ?>
                        <input name="action" type="hidden" id="action" value="update-user" />
                   </div>
                  </div>
                </form><!-- #adduser -->
            <?php endif; ?>
        </div><!-- .entry-content -->
    </div><!-- .hentry .post -->
    <?php endwhile; ?>
<?php else: ?>
    <p class="no-data">
        <?php _e('Sorry, no page matched your criteria.', 'profile'); ?>
    </p><!-- .no-data -->
<?php endif; ?>

		</div><!-- #content -->
</div><!-- #primary -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/edit_profile.js"></script>

<?php get_footer(); ?>


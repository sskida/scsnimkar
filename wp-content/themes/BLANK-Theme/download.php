<?php get_header(); ?>

<?php
/*
Template Name: Download
*/
global $current_user, $wp_roles;

$post_id = $_GET['post_id'];

if(isset($_POST['submit'])) {
    //31454
    $post_id = $_POST['post_id'];
    $password = $_POST['password'];
    $file_password = get_post_meta ( $post_id, '_xxxx_attached_file_password', true );

    if ($password == $file_password) {
        $flag = get_post_meta ( $post_id, '_xxxx_attached_file_download', true );
        if($flag == 'No') {

            $attachments = get_children( array(
                'post_parent'    => $post_id,
                'post_type'      => 'attachment',
                'numberposts'    => -1, // show all -1
                'post_status'    => 'inherit',
                'order'          => 'ASC',
                'orderby'        => 'menu_order ASC'
                ) );
            foreach ( $attachments as $attachment ) {
            $link = wp_get_attachment_url($attachment->ID);
            header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($link));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($link));
    ob_clean();
    flush();
    readfile($link);
update_post_meta($post_id,'_xxxx_attached_file_download','Yes');
wp_redirect( home_url() );
exit;
           }
        }else {
            wp_redirect( home_url().'/download/?post_id='.$post_id.'&err=2' );
            exit;
        }

    } else {
        wp_redirect( home_url().'/download/?post_id='.$post_id.'&err=1' );
        exit;
    }
     
    $aa = get_post_meta ( $post_id, '_xxxx_attached_image', true );
 
    //echo '<pre>';print_r($_POST);exit;
}

?>
<div id="primary" class="site-content">
  <div id="content" role="main">
    <div id="post-<?php the_ID(); ?>">
        <div class="entry-content entry">
           
            <h3 style="text-align:center" >Enter Password For File Download</h3>
            <p class="alert alert-danger" ><?php 
            if(isset($_GET['err'])) {
                switch ($_GET['err']) {
                    case '1':
                        echo 'Password does not match.';
                        break;
                    case '2':
                        echo 'File already downloaded.';
                        break;
                    
                    default:
                        break;
                }
            }
            ?></p>
            <p class="success_msg"></p>
                <form id="_download_password" class="form-horizontal" role="form" method="post" action="<?php the_permalink(); ?>">
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label"><?php _e('Password', 'profile'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="password" type="password" id="password" value="" required />
                             <input class="form-control" name="post_id" type="hidden" id="post_id" value="<?php echo $_GET['post_id']?>" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        <input name="submit"  type="submit" id="submit" class="btn" value="<?php _e('Submit', 'profile'); ?>" />
                   </div>
                  </div>
                </form><!-- #adduser -->
               
        </div>
    </div>
</div>
</div>
  
<?php get_footer(); ?>
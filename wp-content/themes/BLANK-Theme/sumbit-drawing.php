<?php get_header(); ?>

<?php
/*
Template Name: Submit Drawing
*/
global $current_user, $wp_roles;
$sulg = basename(get_permalink());
$cid = $current_user->ID;


if(!$_GET['did'] && !$_GET['msg']) {
    $pid  = ProjectTheme_get_auto_draft($current_user->ID);
    if($sulg == 'submit-drawing'){
        $link = get_permalink()."?did=" . $pid;
        wp_redirect($link);
    }
}else {
     $pid = $_GET['did'];
}
?>

<?php 
if(isset($_POST['submit'])) {
    global $current_user;
    //echo '<pre>';print_r($_POST);exit;
    $parent_cid = $_POST['parent_cid'];
    $user = get_userdata( $parent_cid ); 
    //echo '<pre>';print_r($user);exit;
    $my_post = array(
      'ID'           => $_POST['pid'],
      'post_title'   => 'Circuits Upload By '.$user->user_email,
      'post_author' => $user->ID,
      'post_status' => 'publish',
    );


    // Update the post into the database
    $dd = wp_update_post( $my_post );
    //echo '<pre>';print_r($current_user->display_name);exit;
    $re = $_POST['available_balance'] - $_POST['no_of_ckt'];
    $result = update_user_meta($user->ID, 'credits', $re);
    if($result) {
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        function set_html_content_type() {
            return 'text/html';
        }
        $to = $current_user->user_email;
        $subject = 'New drawing submit';
        $message = "Hi ".$current_user->display_name.",\r\n Thank you for uploading drawing.
         We will reply you as soon as possible. \r\n Thanks & Regards \r\n Team.";
        
        $ss = wp_mail( $to, $subject, $message, $headers ); 
        var_dump($ss);
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

        wp_redirect( get_permalink()."?msg=1");
        exit;

    }
    



}
//echo 'cid--'.$cid.'----pid--'.$pid;
?>

                    <!-- .warning -->
                 

  <div id="primary" class="site-content">
  <div id="content" role="main">

            
    <div >
        <div class="entry-content entry">
<?php 

 if ( !is_user_logged_in() ) : ?>
                    <p class="warning">
                        <?php _e('You must be logged in to submit drawing.'); ?>
                    </p>

<?php else:?>


               <h1 style="text-align:center" >Submit Drawing</h1>
                <form class="form-horizontal" role="form"method="post" id="sub_dr" action="">
                    <div class="form-group">
                        <?php 
                        if($_GET['msg'] == 1) { 
                            echo '<div class="alert alert-success">';
                            echo __('Uploaded drawing successfully.');
                            echo '</div>';
                        }?>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label"><?php _e('No of circuits', 'drwaing'); ?></label>
                        <div class="col-sm-7">
                            <input required class="form-control" name="no_of_ckt" type="text" id="no_of_ckt" value="" />
                            <input name="pid" type="hidden" value="<?php echo $pid; ?>" />
                        </div>
                    </div>


                    <?php 
                    $acc_type = get_user_meta($cid, 'acc_type', 'true');echo '<br>';
                    $cpm_name = get_user_meta($cid, 'cpm_name', 'true');echo '<br>';
                    $arr = explode('|', $cpm_name);
                    $parent_cid = $arr[1];
                    //get_user_meta($parent_cid, 'credits', 'true');exit;
                   //echo $parent_cid = $arr[1];exit;
                    
                    if ($acc_type == 'company-child' || $acc_type == 'company-parent' ) { ?>
                    <div class="form-group" >
                        <label for="last-name" class="col-sm-2 control-label"><?php _e('Available balance', 'drwaing'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="available_balance" type="number" id="available_balance" value="<?php echo get_user_meta($parent_cid, 'credits', 'true');?>" readonly/>
                            <input class="form-control" name="parent_cid" type="hidden" value="<?php echo $parent_cid; ?>"/>
                        </div>
                    </div>
                    <?php }elseif ($acc_type == 'individual') { ?>
                    <div class="form-group" >
                        <label for="last-name" class="col-sm-2 control-label"><?php _e('Available balance', 'drwaing'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" name="available_balance" type="number" id="available_balance" value="<?php echo get_user_meta($cid, 'credits', 'true');?>" readonly/>
                            <input class="form-control" name="parent_cid" type="hidden" value="<?php echo $cid; ?>"/>
                        </div>
                    </div>        
                    <?php }
?>
                    
                    <div class="form-group">
                       <!-- <div class="col-sm-7">
                            <input class="form-control" name="file" type="file" id="dile" value="" />
                        </div> -->
                        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/dropzone.js"></script>     
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/dropzone.css" type="text/css" />
        
                        <script>
 
    
    jQuery(function() {

Dropzone.autoDiscover = false;   
var myDropzoneOptions = {
    maxFilesize: 15,
    addRemoveLinks: true,
    acceptedFiles:'.zip,.pdf,.rar,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.psd,.ai',
    clickable: true,
    url: "<?php bloginfo('siteurl') ?>/?my_upload_of_project_files_proj=1",
};
 
var myDropzone = new Dropzone('div#myDropzoneElement', myDropzoneOptions);

myDropzone.on("sending", function(file, xhr, formData) {
  formData.append("author", "<?php echo $cid; ?>"); // Will send the filesize along with the file as POST data.
  formData.append("ID", "<?php echo $pid; ?>"); // Will send the filesize along with the file as POST data.
});

   
    <?php

        $args = array(
    'order'          => 'ASC',
    'orderby'        => 'menu_order',
    'post_type'      => 'attachment',
    'meta_key'      => 'is_prj_file',
    'meta_value'    => '1', 
    'post_parent'    => $pid,
    'post_status'    => null,
    'numberposts'    => -1,
    );
    $attachments = get_posts($args);
    
    if($pid > 0)
    if ($attachments) {
        foreach ($attachments as $attachment) {
        $url = $attachment->guid;
        $imggg = $attachment->post_mime_type; 
        
        if('image/png' != $imggg && 'image/jpeg' != $imggg)
        {
        $url = wp_get_attachment_url($attachment->ID);
 
            
            ?>

                    var mockFile = { name: "<?php echo $attachment->post_title ?>", size: 12345, serverId: '<?php echo $attachment->ID ?>' };
                    myDropzone.options.addedfile.call(myDropzone, mockFile);
                    myDropzone.options.thumbnail.call(myDropzone, mockFile, "<?php echo bloginfo('template_url') ?>/images/file_icon.png");
                     
            
            <?php
            
      
    }
    }}


    ?>
   


myDropzone.on("success", function(file, response) {
    /* Maybe display some more file information on your page */
     file.serverId = response;
     file.thumbnail = "<?php echo bloginfo('template_url') ?>/images/file_icon.png";
     
       
  });
  
  
myDropzone.on("removedfile", function(file, response) {
    /* Maybe display some more file information on your page */
      delete_this2(file.serverId);
     
  });   
    
    });
    
    </script>
    <?php echo 'Click the grey area below to add project files. Images are not accepted.'; ?>
    <div class="dropzone dropzone-previews" id="myDropzoneElement" ></div>
 
    
    </div>
        </li>
   
        
            <script type="text/javascript">
    
    function delete_this2(id)
    {
         jQuery.ajax({
            method: 'get',
            url : '<?php echo get_bloginfo('siteurl');?>/index.php/?_ad_delete_pid='+id,
            dataType : 'text',
            success: function (text) {   jQuery('#image_ss'+id).remove();  }
         });
    }

    </script>
                    </div>
                            
                    

                  
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        
                        <input style="margin-bottom: 10px !important;"  name="submit"  type="submit" id="updateuser" class="btn" value="<?php _e('Upload', 'profile'); ?>" />
                        
                   </div>
                  </div>
                </form><!-- #adduser -->
            
        </div><!-- .entry-content -->
    </div><!-- .hentry .post -->
   

    </div><!-- #content -->
</div><!-- #primary -->
 <?php endif;?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/edit_profile.js"></script>


<script type="text/javascript">

$("#sub_dr").submit(function(){ 
    var ckt = jQuery('#no_of_ckt').val();
        var b_ckt = jQuery('#available_balance').val();

        if(ckt > b_ckt) {
            alert("Insufficient balance");
            return false;
        }else {
            return true;
        }
});
</script>













<?php get_footer(); ?>
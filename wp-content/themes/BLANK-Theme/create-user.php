<?php 
/*
Template Name: Create User 
*/
?>

<?php get_header(); 

global $current_user, $wp_roles;
$user_id = $current_user->ID
?>
               

<div id="primary" class="site-content">
    <div id="content" role="main">
        <div class="entry-content entry">
            <div style="width: 50%;" id="box-shadow" class="clearfix">
            <div class="account-box" id="sign-tool"  >             
            <h3 class="header_text"> CREATE USER</h3>             
            <div>
            <p class="error_msg"></p>
            <p class="success_msg"></p>
            </div>
            <form  action="<?php echo wp_login_url(); ?>" method="post" class="loginForm" id="signup-child">
            
            <p class="form-group">
                <input type="hidden" name="role" id="role-child" value="subscriber" />
                <input type="hidden" name="acc_type" id="acc_type-child" value="company-child" />
            
            </p>
                
            <p class="form-group">
            <input type="text" name="name" placeholder="Name" class="form-control" tabindex="1" id="name-child" required value="<?php if (isset($_POST['name'])) echo esc_attr( $_POST['name'] ); ?>" />
            </p>

            <p class="form-group">
            <input type="hidden" id="cpm_name-child" name="cpm_name" required value="<?php echo get_user_meta($user_id, 'cpm_name', 'true').'|'.$user_id  ; ?>" />
            </p>

            <p class="form-group">
            <input type="number" placeholder="Mobile Number" maxlength="10" name="mobile" class="form-control" tabindex="3" id="mobile-child" required value="<?php if (isset($_POST['mobile'])) echo esc_attr( $_POST['mobile'] ); ?>" />
            </p>

            <p class="form-group">
            <input type="email" name="email" placeholder="Email" class="form-control" tabindex="4" id="email-child" required value="<?php if (isset($_POST['email'])) echo esc_attr( $_POST['email'] ); ?>" />
            </p>

                        
            <p class="form-group">
            <input type="password" placeholder="Password" name="password" class="form-control" tabindex="6" id="password-child" value="" required/>
            </p> 

            <p class="form-group">
            <input type="password" placeholder="Confirm Password" name="cfm_password" class="form-control" tabindex="7" id="cfm_password-child" value="" required/>
            </p> 

            <button tabindex="8" type="submit" class="btn btn-lg btn-block" id="sign-btn1" name="signup" >Create User</button>
            </form>
            

            <?php //echo appthemes_get_password_recovery_url(); ?>

            </div>           
               

            
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
jQuery(document).on("submit", '#signup-child',function(e){
        e.preventDefault();
        jQuery('.error_msg').text('Loading...').fadeIn();
        var role = jQuery('#role-child').val();
        var acc_type = jQuery('#acc_type-child').val();
        var name = jQuery('#name-child').val();
        var cpm_name = jQuery('#cpm_name-child').val();
        var mobile = jQuery('#mobile-child').val();
        var email = jQuery('#email-child').val();
        var password = jQuery('#password-child').val();
        var cfm_password = jQuery('#cfm_password-child').val();
        
        jQuery.ajax({
            type: 'POST',
            context: this,
            url: ajaxurl,
            data: {action: 'ajax_signup','acc_type':acc_type,'name':name,'cpm_name':cpm_name,'mobile':mobile,'email':email,'password':password},
            success:function(data){
                jQuery('.error_msg').fadeOut();
                if(jQuery.trim(data) == jQuery.trim('success')){
                    jQuery('.success_msg').text('Please check email for account activation').fadeIn();  
                    location.reload();   
                }
                else{
                    jQuery('.error_msg').text(data).fadeIn();   
                }
            }
        });  
    });

$(function() { 
  $("#role").on("change",function() { 
    //$("#inp").val($(this).val());   company-parent
   var a = $(this).val();
   if(a == 'subscriber')
   {
        var s = 'individual-parent';
        document.getElementById("acc_type").value = s;
   }
   if(a == 'contributor')
   {
        var s = 'company-parent';
        document.getElementById("acc_type").value = s;
   }
  }); 
}); 

</script>
<?php get_footer(); ?>
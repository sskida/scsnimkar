<?php 
/*
Template Name: Customer Info
*/
?>

<?php get_header(); 

if($_GET['cid']) {
    $course = get_post($_GET['cid']);
    $fees = get_post_meta( $course->ID, 'courses_fee', true );
}

?>
<?php
$today = date('Y-m-d');
$args = array(
    'meta_query' => array(
        array(
            'key' => 'batch_start',
            'compare'   => '>',
            'value'     => $today,
        )
    ),
    'post_type' => 'batches',
    'posts_per_page' => -1
);
$posts = get_posts($args);

//echo '<pre>';print_r($posts);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
       <?php echo do_shortcode( '[huge_it_slider id="1"]' ) ?>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="tab-content">
                            <form method="POST" action="<?php echo get_template_directory_uri().'/shop/payseal.php'?>">
                                    
                                    <div class="row">
                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            
                                        </div>
                                    
                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><label>Customer Name*<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="customername" type="text" name="customername" value="" required>
                                            </p>
                                        </div>
                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><label>Course Name*<label></p>
                                            <p  class="tradesmen-signup-input">
                                                <input class="form-control" id="course_name" type="text" name="course_name" readonly value="<?php echo $course->post_title; ?>" required>
                                            </p>
                                        </div>
                                    
                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><p><label>Course Fees*<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="course_fees" type="text" name="course_fees" readonly value="<?php echo $fees; ?>" required>
                                            </p>
                                        </div>

                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><p><label>Batch start date*<label></p>
                                            <select class="form-control" name="can_batch" required>
                                            <option value="">Select Start Date</option>
                                            <?php foreach ($posts as $post) { 
                                            $batch_start = get_post_meta($post->ID,'batch_start',true);
                                                ?>
                                            <option value="<?php echo $batch_start?>"><?php echo $batch_start?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="tab-content_col col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><label>Email*<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="tradesmen_email" type="email" name="email" value="" required>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="tab-content_col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <p><label>Address<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="address" type="text" name="address" value="">
                                            </p>
                                        </div>
                                        <div class="tab-content_col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <p><label>Street<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="street" type="text" name="street" value="">
                                            </p>
                                        </div>
                                        <div class="tab-content_col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <p><label>City<label></p>
                                            <p class="tradesmen-signup-input">
                                                <input class="form-control" id="city" type="text" name="city" value="">
                                            </p>
                                        </div>
                                        <div class="tab-content_col col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <p><label>Postal Code<label></p>
                                            <p class="customer-signup" style="text-align:center">
                                                <input class="form-control" id="postcode" type="text" name="postcode" value="">
                                            </p>
                                        </div>
                                    </div>
                                
                                <div id="sectionE" class="tab-pane ">
                                    <div class="row">
                                        <div style="text-align:center;padding-bottom: 10px;" id="personal_details_submit_col" class="hover tab-content_col  col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <input class="btn" id="thank_you_submit" type="submit" value="SAVE">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    </div>
</div>
<?php get_footer(); ?>
<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'cscnimkar');
define('DB_NAME', 'scsnimka_wpscs');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',8$!a.|A;UOuS03ltf0GmvyAb#Z{,&Giv;V<9zE2q |-CBXhg iC6m9MiX+t<ie8');
define('SECURE_AUTH_KEY',  'XN2A/f1-1rN1]O2jpHZz|Oy[`1-Pi^K-Kz1c[`<GK^5X}lQROjyVFLb?Dq?ZFYjQ');
define('LOGGED_IN_KEY',    '}3(?6NcZpiXe`C ^Fy9R*xa~=YzqJ^vHZ|,?_#GA-@gdzyF<bDg^j D+M@h_}MI}');
define('NONCE_KEY',        'uQ6`)1RgYO|(DjhU{_ZLefbx7-&5:e|Wi~+N+rYXZ1<u+x`YCK4|Ko?S-jsx/%3E');
define('AUTH_SALT',        'T+BND4xKD~t*0{)v3MPzF,<-3tz.SwN[oJp+Kj+xkCywoC7(_7<f!nIBt{wne>86');
define('SECURE_AUTH_SALT', 'i^U#fxqx|o|0x gK+%Kc=fgK=EJGX~=uh<BN*e-JXEPLu;Q@()%msaLu13%9$&}5');
define('LOGGED_IN_SALT',   'oc.D-=QtO?kkt^jh3)`d6:Z8W3dX,uAg[1^%p`~??+A1>li9^rQsF+]6K@L)y;52');
define('NONCE_SALT',       'I2[wom,+<GsCs]rm-B82-}-V#}Rqdxkdir})1Vi)-PoA[O!e^zAZC*#5O6fF@{tW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
